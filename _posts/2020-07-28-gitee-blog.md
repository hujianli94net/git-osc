---
layout: post
title: "CI 工具「Gitee Go」现已进入公测，点击领取免费时长"
---

<p>Gitee 在今年 4 月份推出了 CI 服务「Gitee Go」，为开发者们又增添了一个 CI 服务可选项，目前「Gitee Go」已支持 Maven、Gradle、npm、Python、Ant、PHP、Golang 等工具和语言的编译和构建工作。</p>

<p><img alt="" src="https://oscimg.oschina.net/oscnet/up-a8740cb6c73e9c06db00fc629161bdc07b5.png" width="700" /></p>

<p><span style="background-color:#ffffff; color:#494949">用户可通过 YAML 语法编写流水线文件</span>，如果对于 YAML 语法不太熟悉，我们也提供了多套示例文件，用户可根据示例文件进行修改，自定义构建脚本创建流水线，进行代码仓库的编译和构建。</p>

<p><img alt="" height="442" src="https://oscimg.oschina.net/oscnet/up-4c5cabeaa49c6f21840403489660cc5e92b.png" width="700" /></p>

<p>在三个多月的内测期间，「Gitee Go」也收到了参与内测企业的众多反馈及建议。经过完善，「Gitee Go」现正式进入公测阶段，开放给所有企业版用户（免费版及以上版本），并为每个企业提供 <strong>200 分钟的免费构建时长</strong>。</p>

<p>用户只需登录自己的企业，依次选择「管理」--&gt;「扩展应用」--&gt;「Gitee Go」，选择页面上方的「构建时长免费领」，即可获取 200 分钟免费时长。（领取时间：即日起至 2020 年 9 月 30 日）</p>

<p><img alt="" src="https://oscimg.oschina.net/oscnet/up-f8c5b8be43caff074e4a12eb6492c400605.gif" width="700" /></p>

<p>同时，「Gitee Go」也开启了购买通道，支持用户按需购买构建时长。</p>

<p><img alt="" height="293" src="https://oscimg.oschina.net/oscnet/up-65139480b9eac428c03fedbe2b21d7c10d3.png" width="700" />&nbsp;</p>

<p>未来，「Gitee Go」除了更多开发语言和工具的支持外，持续部署以及制品（构建物）的管理也将会陆续上线。</p>

<p>欢迎各位 Gitee 企业版用户前来试用「Gitee Go」，如果您在使用过程遇到问题或者有任何的建议，可以加入「Gitee Go」QQ技术交流群：1078759826，和 Gitee 团队面对面交流。</p>
