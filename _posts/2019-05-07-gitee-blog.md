---
layout: post
title: "代码被删还遭勒索，怎么防？"
---

<p>5月4日，GitHub 的 多名用户受到了攻击，仓库中的源代码全部消失，只留下了一封缴纳比特币赎金的勒索信！</p>

<p><img height="auto" src="https://static.oschina.net/uploads/img/201905/07102646_Km5V.jpg" width="auto"></p>

<p>信中表示，他们已经将源代码下载并存储到了自己的服务器上。受害者要在10天之内，往特定账户支付0.1比特币（约合人民币3800元），否则他们将公开代码，或以其他的方式使用它们。部分&nbsp;GitLab、Bitbucket、<strong>码云</strong>用户也表示遇到同样问题。</p>

<p><strong>代码仓库遭到攻击的可能原因：</strong></p>

<p>1、以明文形式存储在相关代码库的部署中</p>

<p>2、密码简单/弱口令，遭到暴力破解</p>

<p>373名用户，虽然相对于上千万开发者用户来说仅仅是九牛一毛，但代码仓库安全仍不容忽视。</p>

<p>&nbsp;</p>

<p><strong>代码宝贵，Gitee 提醒您：</strong></p>

<p>1、使用 SSH 密钥提高安全性</p>

<p>2、使用强密码，不要在代码中明文体现密码</p>

<p>3、开启微信登录提醒，第一时间掌控账号动态</p>

<p>请拿起手机，使用微信扫描一下码云微信服务号<strong>「码云Gitee」</strong></p>

<p><img height="auto" src="https://static.oschina.net/uploads/img/201905/07102647_2ck6.jpg" width="153px"></p>

<p>点击菜单<strong>「我的」</strong>-&gt;<strong>「个人主页」</strong></p>

<p>输入你的码云账号密码</p>

<p>即可完成码云账号绑定，开启码云微信通知之旅！</p>

<p><img alt="" height="353" src="https://static.oschina.net/uploads/space/2019/0507/102205_dKBs_12.jpg" width="600"></p>

<p>&nbsp;</p>

<p>账号各类安全信息第一时间全握在手，稳！</p>

<p>还能即时接收项目/Issue动态，使用微信快捷登录码云，很方便了：）</p>

<p><strong>进阶版安全策略——使用</strong><a href="https://gitee.com/enterprises" target="_blank"><strong>码云企业版</strong></a><strong>管理重要代码</strong></p>

<p>不仅支持敏感操作二次验证，还可禁止强推，仓库自动备份，省心更安心。</p>
