---
layout: post
title: "码云「邮箱隐私保护」和「禁止命令行推送暴露个人邮箱」 功能上线"
---

<p>今年年初，码云支持了一个帐号多邮箱的特性（详见<a href="https://www.oschina.net/news/104561/gitee-account-with-multiple-email">《你到底有几个邮箱？码云账号增加多邮箱支持！》</a>），支持用户将多个邮箱绑定到码云帐号，同时支持用户通过 git config 配置形如 `xxxx+username@user.noreply.gitee.com`&nbsp; 的邮箱进行代码提交并产生贡献度。</p>

<p>出于隐私保护的考虑，码云后续又推出了「邮箱隐私保护」的功能。用户在「个人设置」-&gt;「基本设置」-&gt;「多邮箱管理」中开启了「不公开我的邮箱地址」后，在网页上修改仓库内容并产生提交时，系统将默认使用上述的隐私邮箱地址作为提交邮箱。通过这一功能，用户可以有效的保护自己的邮箱信息以避免不必要的打扰。</p>

<p><img height="1106" src="https://static.oschina.net/uploads/space/2019/1107/161126_rsyo_12.png" width="2527" /></p>

<p>现码云已支持「禁止命令行推送暴露个人邮箱」功能，在用户开启了「不公开我的邮箱地址」后，系统会默认会在用户推送代码时对推送的提交信息进行检测，如果包含当前用户已设置不公开的邮箱提交信息，系统会默认拒绝推送并提示</p>

<blockquote>
<p>remote: Powered by GITEE.COM [GNK-3.7]<br />
remote: error: GE007: Your push would publish a private email address.<br />
remote: You can make your email public or disable this protection by visiting:<br />
remote: https://gitee.com/profile/emails</p>
</blockquote>

<p>如下图：</p>

<p><img height="290" src="https://oscimg.oschina.net/oscnet/97e4554cc6efb5b94ae0350e7c80b137206.jpg" width="636" /></p>

<blockquote>
<p>如果用户推送失败，看到上面的提示信息。可以通过对应码云帐号，在「个人设置」-&gt;「基本设置」-&gt;「多邮箱管理」中取消「禁止命令行推送暴露个人邮箱」选项勾选即可。</p>
</blockquote>

<p>即刻前往&nbsp;<a href="https://gitee.com/profile/emails">https://gitee.com/profile/emails</a>&nbsp;即可体验「邮箱隐私保护」功能</p>
