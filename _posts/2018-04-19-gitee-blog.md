---
layout: post
title: "快讯 —— 码云企业版个人工作台上线"
---

<p>码云企业版的个人工作台页面上线啦 ！！！</p><p>更直观的查看个人任务和项目，界面如下：</p><p><img src="https://static.oschina.net/uploads/space/2018/0419/105838_mthQ_12.jpg"/></p><p>这是码云企业版 4.0 的一部分，接下来我们还会继续优化任务、项目和文档管理，欢迎关注。</p><p>现在就去体验&nbsp;<a href="https://gitee.com/enterprises?from=news" target="_self" textvalue="https://gitee.com/enterprises">https://gitee.com/enterprises</a></p>