---
layout: post
title: "每日一博 | 浅谈 Pull Request 与 Change Request 研发协作模式"
---

<blockquote>
<p>说起 PullRequest 相信大部分人都不会陌生，它是由 Github 推出的一种开源协作模式，由于 Gitlab 占据着企业内部私有部署的半壁江山，这种模式也更多的用在企业内部代码审核流程，也就是所谓的 CodeReview。其实还有很多企业和团队会选择 Gerrit 这个工具，Gerrit 提供的是 ChangeRequest 模式，这种模式更具有针对性，对代码审核的粒度也更细，近期有客户需求在 Gitee 上实现类似 ChangeRequest 的需求，所以针对两种模式做一个介绍，探讨两种模式的具体适用场景。</p>
</blockquote>
