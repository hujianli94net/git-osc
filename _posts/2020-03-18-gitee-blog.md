---
layout: post
title: "Gitee 项目拓扑图 —— 密集恐惧症患者谨慎进入"
---

<p>公司里有那么多人，那么多项目，那么多仓库，还有几个部门。他们之间的关系是怎样的？ 晕不晕？</p>

<p>我们换一种方式来展示这些对象之间的关系：</p>

<p><img alt="" src="https://oscimg.oschina.net/oscnet/up-249a791c8d95659998aa530bd16c9168a47.JPEG" /></p>

<p>是不是更晕了？ 这就是典型的密集恐惧症啊！</p>

<p><img src="https://static.oschina.net/uploads/img/202003/18065342_ehWT.jpg" /></p>

<p>这是开源中国的项目拓扑图，我们公司一百多号人，七百多个仓库，看起来就是密密麻麻如蜘蛛网般。&nbsp;</p>

<p>点击其中一个项目（如下图），这回要舒服多了吧？</p>

<p><img alt="" src="https://oscimg.oschina.net/oscnet/up-d503f26f74c84e29d831d2abedbffcf1ab8.JPEG" /></p>

<p>这就是 Gitee 企业版新的小特性 &mdash;&mdash; 项目拓扑图，通过图的方式来展示项目、团队、成员、仓库之间的逻辑关系。</p>

<p>你可以进入 Gitee 企业版 -&gt; 项目 -&gt; 项目拓扑图 查看。</p>

<p>更多 Gitee 企业版特性请前往：<a href="https://gitee.com/enterprises">https://gitee.com/enterprises</a></p>
