---
layout: post
title: "11 月入选码云 GVP 最有价值开源项目一览表"
---

<p><strong>GVP (&nbsp;Gitee Most&nbsp;Valuable&nbsp;Project ) - 码云最有价值开源项目计划&nbsp;</strong>是码云综合评定出的优秀开源项目的展示平台。GVP 项目的评选既要满足客观的硬指标，还需要通过评委会主观的投票认可。经过 2 年多时间，目前 Gitee 的 GVP 项目已有两百多个。</p>

<p>下面是 2019年11月最新入选 GVP 的项目列表（安装入选时间先后顺序排列）：</p>

<h3><a href="https://gitee.com/wallace5303/dapps">Dapps</a></h3>

<p>一个应用程序商店，包含丰富的软件，因为基于docker，使你本机电脑有云开发的效果。 一键安装程序；多版本共存，完善的使用说明，且不影响本机环境。 前端、服务端、运维、站长可以直接使用，效率提高非常多。普通用户亦可使用其中部分软件</p>

<ol>
	<li>
	<p><img alt="" src="https://i.loli.net/2019/10/11/yWCI8TQReAMsdpB.png" /></p>
	</li>
	<li>&nbsp;</li>
</ol>

<p>授权许可：Apache</p>

<h3><a href="https://gitee.com/freshday/radar">Radar</a></h3>

<p>实时风控引擎(Risk Engine)，自定义规则引擎(Rule Script)，完美支持中文，适用于反欺诈(Anti-fraud)应用场景，开箱即用！！！移动互联网时代的风险管理利器</p>

<p>&nbsp;</p>

<p><img alt="系统模块" src="http://www.pgmmer.top/radar/sys_model_arch.png" /></p>

<p>授权许可：Apache</p>

<h3><a href="https://gitee.com/fastdfs100/fastdfs">FastDFS</a></h3>

<p>FastDFS是一款轻量级的开源分布式文件系统，功能包括：文件存储、文件同步、文件上传、文件下载等，解决了文件大容量存储和高性能访问问题。特别适合以文件为载体的在线服务，如图片、视频、文档服务等等</p>

<p>授权许可：GPLv3.0</p>

<h3><a href="https://gitee.com/actionview/av">ActionView</a></h3>

<p>ActionView 是一个类Jira的问题需求跟踪工具。一直在用Jira进行任务管理和Bug跟踪，除了采购License价格不菲外，使用过程中觉得Jira还是有点重、全局方案配置到了后期越来越难维护、页面体验也不像现在流行的SPA那么好，所以有了做ActionView的想法</p>

<p><img alt="image" src="http://actionview.cn/images/kanban.png" /></p>

<p>授权许可：Apache</p>

<h3><a href="https://gitee.com/hyperf/hyperf">Hyperf</a></h3>

<p>Hyperf 是基于&nbsp;<code>Swoole 4.4+</code>&nbsp;实现的高性能、高灵活性的 PHP 协程框架，内置协程服务器及大量常用的组件，性能较传统基于&nbsp;<code>PHP-FPM</code>&nbsp;的框架有质的提升，提供超高性能的同时，也保持着极其灵活的可扩展性，标准组件均基于&nbsp;<a href="https://www.php-fig.org/psr">PSR 标准</a>&nbsp;实现，基于强大的依赖注入设计，保证了绝大部分组件或类都是&nbsp;<code>可替换</code>&nbsp;与&nbsp;<code>可复用</code>&nbsp;的。</p>

<p>授权许可：MIT</p>

<h3><a href="https://gitee.com/rtttte/Archery">Archery</a></h3>

<p>Archery 定位于 SQL 审核查询平台，旨在提升 DBA 的工作效率，支持主流数据库的 SQL 上线和查询，同时支持丰富的 MySQL 运维功能</p>

<p><img alt="" src="https://images.gitee.com/uploads/images/2019/1110/202317_32bd4a1c_1038040.png" /></p>

<p>授权许可：Apache&nbsp;</p>

<h3><a href="https://gitee.com/LiteOS/LiteOS">LiteOS</a></h3>

<p>Huawei LiteOS一直致力于打造易用、安全、高性能的物联网系统，项目迁入码云(Gitee)后，LiteOS开源团队将开展社区项目更新，包括丰富的组件包、新平台的支持、配套工程工具。敬请关注，期待大家的参与。</p>

<p><img alt="" src="https://gitee.com/LiteOS/LiteOS/raw/master/doc/meta/DevGuide/pic1.png" /></p>

<p>授权许可：BSD3</p>

<h3><a href="https://gitee.com/baidu/apolloauto">Apollo</a></h3>

<p>Apollo (阿波罗)是一个开放的、完整的、安全的平台，将帮助汽车行业及自动驾驶领域的合作伙伴结合车辆和硬件系统，快速搭建一套属于自己的自动驾驶系统</p>

<p><img alt="image alt text" src="https://gitee.com/baidu/apolloauto/raw/master/docs/demo_guide/images/Apollo_1.png" /></p>

<p>授权许可：Apache</p>

<h3><a href="https://gitee.com/sofastack/sofa-tracer">SOFATracer</a></h3>

<p>SOFATracer 是一个用于分布式系统调用跟踪的组件，通过统一的 traceId 将调用链路中的各种网络调用情况以日志的方式记录下来，以达到透视化网络调用的目的。这些日志可用于故障的快速发现，服务治理等。</p>

<p>授权许可：Apache</p>

<p>==========================</p>

<p>更多的 GVP 项目请浏览&nbsp;<a href="https://gitee.com/gvp">https://gitee.com/gvp</a>&nbsp;网页。</p>

<p>此外，2019 最受欢迎国产开源项目的评选进行如火如荼，欢迎投出你宝贵一票&nbsp;<a href="https://www.oschina.net/project/top_cn_2019?utm_source=gvp">https://www.oschina.net/project/top_cn_2019</a></p>

<p>最后，所有的 GVP 项目的开发者都可以免费参加开源中国源创会，2019年开源中国年终盛典正在报名中，欢迎大家报名参加：<a href="https://www.oschina.net/event/2312491">https://www.oschina.net/event/2312491</a></p>

<p><span style="color:#c0392b"><strong>如果你觉得这些项目还不错，别忘了给它们个 Star 哦！</strong></span></p>

<ul>
</ul>

<ul>
</ul>
