---
layout: post
title: "Gitee 轻量级 PR —— 参与开源贡献，原来就是举手之劳的事"
---

<p><img alt="" height="314" src="https://static.oschina.net/uploads/space/2020/0313/074050_d1eS_12.jpg" width="600" /></p>

<p><code>Github </code>&nbsp;发明了&nbsp; <code>Pull Requests </code>&nbsp;，让全球范围内的开源协作变得如此简单。任何人不需要联系作者，只需简单四步即可提交贡献代码给项目：</p>

<ol>
	<li><code>Fork </code>&nbsp;主仓库到自己账号成为副本仓库</li>
	<li>在副本仓库完成代码贡献（添加、删除、修改代码等等）</li>
	<li>将副本修改的内容给主仓库提交 PR ( <code>Pull Requests </code>)</li>
	<li>作者审核你提交的代码，并决定是否合并</li>
</ol>

<p>这种简单的协作模式让开源软件发展非常非常快速。</p>

<p>但参与开源贡献并非一件简单的事，你必须对项目有着非常深入了解。可有时候我们可能只是想帮作者纠正一些文档的错误，或者一些不易发掘的代码错误等等。那么这个 Pull Requests 的操作就有点复杂，会因此打消很多人贡献的想法。</p>

<p><span style="color:#c0392b">为了降低开源贡献的门槛，Gitee 推出了 <code>轻量级PR </code>的功能。</span></p>

<p>具体使用流程如下：</p>

<h4>1. 打开任意的开源项目，例如&nbsp;<a href="https://gitee.com/ld/J2Cache">https://gitee.com/ld/J2Cache</a></h4>

<h4>2. 点击任何你发现问题的文件，并直接进入文件编辑（如下图）<br />
<img alt="输入图片说明" src="https://images.gitee.com/uploads/images/2020/0313/072354_8a59c366_36.png" width="400/" /></h4>

<h4>3. 完成你想要修改的内容，输入修改的说明，点击&ldquo;提交审核&rdquo;按钮<br />
<img alt="输入图片说明" src="https://images.gitee.com/uploads/images/2020/0313/072523_b517365f_36.png" width="500/" /></h4>

<h4>4. 完事了，接下来就等着作者审核</h4>

<p>这时作者会收到一个 PR 的信息，按照普通的 PR 进行审核即可。<br />
<img alt="输入图片说明" src="https://images.gitee.com/uploads/images/2020/0313/072735_4db21d15_36.png" width="400/" /></p>

<p>Gitee 的轻量级 PR 在 Gitee IDE 中也是支持的，你可以通过仓库页面的 Web IDE 进入，修改多个文件并提交轻量级PR，有兴趣的朋友可以试试看。</p>
